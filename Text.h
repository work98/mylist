#pragma once
#include "List.h"
class Text {
	List<List<char>*> lines = List<List<char>*>();
public:
	Text() {}
	Text(char* text) {
		long i = 0;
		auto line = new List<char>();
		while (text[i]!='\0') {
			if (text[i] != '\n') {
				line->Add(text[i]);
			}
			else {
				lines.Add(line);
				line = new List<char>();
			}
			i++;
		}
	}
	int LinesCount() {
		return lines.Count();
	}
	List<char> get(int line) {
		return *lines.Get(line);
	}
};