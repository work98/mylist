#include "ListChain.h"
#include "List.h"
#include <iostream>
using namespace std;
int main() {
	auto list = List<int>();
	list.Add(1);
	list.Add(2);
	list.Add(3);
	list.PushFirst(4);
	list.PushAt(2, 10);
	cout << list.Get(0) << endl;
	cout << list.Get(1) << endl;
	cout << list.Get(2) << endl;
	cout << list.Get(3) << endl;
	cout << list.Get(4) << endl;
	cout << list.Get(5) << endl;


	return 1;
}