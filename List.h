#include "ListChain.h"


template <typename T>
class List {
private:
	ListChain<T>* first;
public:
	List() {

	}
	T Last() {
		ListChain<T>* curr = first;
		while (curr->next != nullptr)
			curr = curr->next;
		return curr;
	}
	T First() {
		ListChain<T>* curr = first;
		if (curr != nullptr)
			return curr;
		return nullptr;
	}
	void Add(T element) {
		if (first == nullptr) {
			first = new ListChain<T>(element);
			return;
		}

		ListChain<T>* curr = first;
		while (curr->next != nullptr)
			curr = curr->next;
		curr->next = new ListChain<T>(element);
	}

	void PushAt(int idx, T element) {
		int c = 1;
		ListChain<T>* curr = first;
		auto a = this->Count();
		if (idx >= this->Count()) return;
		if (first == nullptr) return;
		while (curr->next != nullptr && c != idx) {
			curr = curr->next;
			c++;
		}
		auto buff = curr->next;
		curr->next = new ListChain<T>(element);
		curr->next->next = buff;
	}

	void PushFirst(T element) {
		auto first = this->first;
		this->first = new ListChain<T>(element);
		this->first->next = first;
	}

	int Count() {
		if (first == nullptr) return 0;
		int c = 1;
		ListChain<T>* curr = first;
		while (curr->next != nullptr) {
			curr = curr->next;
			c++;
		}
		return c;
	}
	T Get(int idx) {
		int c = 0;
		ListChain<T>* curr = first;
		auto a = this->Count();
		if (idx >= this->Count()) return (T)nullptr;
		if (first == nullptr) return (T)nullptr;
		while (curr->next != nullptr && c != idx) {
			curr = curr->next;
			c++;
		}
		return curr->value;
	}


};