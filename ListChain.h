#pragma once
template <typename T>
class ListChain {
public:
	T value;
	ListChain* next;

	ListChain(T value) {
		this->value = value;
		this->next = nullptr;
	}

};